package cards

import "fmt"

type (
	CardSuit uint
	CardRank uint
)

const (
	Diamonds CardSuit = iota
	Hearts
	Spades
	Clubs
)

const (
	Seven CardRank = iota
	Eight
	Nine
	Queen
	King
	Ten
	Ace
	Jack
)

// Card represents a card in the game a card has a suit and a rank
// Each card has specific points based on the suit and rank
type Card struct {
	Suit   CardSuit
	Rank   CardRank
	Points int
}

// Create a new card based on a given suit and rank
func New(suit CardSuit, rank CardRank) (*Card, error) {
	points := 0
	switch rank {
	case Jack:
		points = 2
	case Queen:
		points = 3
	case King:
		points = 4
	case Ten:
		points = 10
	case Ace:
		points = 11
	default:
		points = 0
	}
	if (suit < Diamonds) || (suit > Clubs) {
		return nil, fmt.Errorf("invalid color provided: %d", suit)
	}
	if (rank < Seven) || (rank > Jack) {
		return nil, fmt.Errorf("invalid rank provided: %d", rank)
	}
	return &Card{Suit: suit, Rank: rank, Points: points}, nil
}

var (
	Colors = []CardSuit{Diamonds, Hearts, Spades, Clubs}
	Ranks  = []CardRank{Seven, Eight, Nine, Queen, King, Ten, Ace, Jack}
)

func (r CardRank) Compare(r2 CardRank) int {
	if r < r2 {
		return -1
	} else if r > r2 {
		return 1
	}
	return 0
}

func (c *Card) Compare(c2 *Card) int {
	rankCmp := c.Rank.Compare(c2.Rank)
	result := 0
	if rankCmp != 0 {
		result = rankCmp
	}

	if c.Suit < c2.Suit {
		result = -1
	}

	if c.Suit > c2.Suit {
		result = 1
	}
	return result
}

func (c *Card) IsEqualTo(c2 *Card) bool {
	return c.Compare(c2) == 0
}

func (c *Card) IsLessThan(c2 *Card) bool {
	return c.Compare(c2) < 0
}

func (c *Card) IsGreaterThan(c2 *Card) bool {
	return c.Compare(c2) > 0
}
