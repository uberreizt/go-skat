package cards_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/uberreizt/go-skat/cards"
)

func TestCardComparison(t *testing.T) {
	aceOfSpades, err := cards.New(cards.Spades, cards.Ace)
	assert.Nil(t, err, "Unable to create an Ace of Spades")

	aceOfClubs, err := cards.New(cards.Clubs, cards.Ace)
	assert.Nil(t, err, "Unable to create an Ace of Clubs")

	anotherAceOfSpades, err := cards.New(cards.Spades, cards.Ace)
	assert.Nil(t, err, "Unable to create another Ace of Spades")

	assert.True(t, aceOfSpades.IsEqualTo(anotherAceOfSpades), "Two Cards of the same Type needs to be equal")
	assert.True(t, aceOfSpades.IsLessThan(aceOfClubs), "The Ace of spades does have a lower rank than the ace of clubs")
	assert.True(t, aceOfClubs.IsGreaterThan(aceOfSpades), "The Ace of clubs does have a higher rank than the ace of spades")
}
