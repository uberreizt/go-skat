# Running a local Go version

Assuming that there is a `$HOME/.golang` folder holding multiple Go versions and a $HOME/.gopath` holding multiple Go Path for the versions.

```
export PATH=$HOME/.golang/1.19.1/bin:$PATH
export GOPATH=$HOME/.gopath/1.19.1/
export PATH=$PATH:$(go env GOPATH)/bin
```

# Running the Test

In the toplevel project folder run

```
go test ./...
```

This will run all tests for you.

# Godoc - Run the documentation server

## Install godoc

```
go install golang.org/x/tools/cmd/godoc
```

## Run the server

```
godoc -http=:6060
```