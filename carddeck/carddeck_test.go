package carddeck_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/uberreizt/go-skat/carddeck"
)

const (
	skatCardDecSize = 32
)

func TestCardDeckLength(t *testing.T) {
	cardDec := carddeck.New()

	assert.Equal(t, skatCardDecSize, len(cardDec),
		fmt.Sprintf("For a Skat card deck we need %d Cards", skatCardDecSize))
}

func TestDeckOrdering(t *testing.T) {
	cardDec := carddeck.New()
	for i := 0; i < len(cardDec)-1; i++ {
		assert.True(t,
			cardDec[i].IsLessThan(&cardDec[i+1]),
			fmt.Sprintf("The Order in a sorted card dec needs to be ascending %#v vs. %#v",
				cardDec[i],
				cardDec[i+1]))
	}
	for i := 1; i < len(cardDec); i++ {
		assert.True(t,
			cardDec[i].IsGreaterThan(&cardDec[i-1]),
			fmt.Sprintf("The Order in a sorted card dec needs to be ascending %#v vs. %#v",
				cardDec[i],
				cardDec[i-1]))
	}
}
