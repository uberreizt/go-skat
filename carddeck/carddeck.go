package carddeck

import (
	"gitlab.com/uberreizt/go-skat/cards"
)

func New() []cards.Card {
	cardDec := []cards.Card{}
	for _, color := range cards.Colors {
		for _, rank := range cards.Ranks {
			card, err := cards.New(color, rank)
			if err == nil {
				cardDec = append(cardDec, *card)
			}
		}
	}
	return cardDec
}

func Shuffle(dec *[]cards.Card) *[]cards.Card {
	cardDec := []cards.Card{}
	for _, color := range cards.Colors {
		for _, rank := range cards.Ranks {
			card, err := cards.New(color, rank)
			if err == nil {
				cardDec = append(cardDec, *card)
			}
		}
	}
	return &cardDec
}
